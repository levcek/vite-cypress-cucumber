# React, TS, Vite, and Cucumber setup

This procedure is a combination of a **React Typescript Vite app** scaffolding, **Cypress installation**, and **[the guide on how to setup the cypress-cucumber-preprocessor](https://github.com/badeball/cypress-cucumber-preprocessor/blob/master/docs/quick-start.md)**. I just combined these into one flow and surmounted some gotchas for you.

This works on Chrome 107 and Firefox 107.

## Steps

 1. `npm create vite@latest`
 2. Choose React
 3. Choose Typescript
 4. `npm i cypress`
 5. `npm i @badeball/cypress-cucumber-preprocessor --save-dev`
 6. `npm i @bahmutov/cypress-esbuild-preprocessor --save-dev`
 7. `npm i esbuild --save-dev`
 
 Following this [Quick start guide](https://github.com/badeball/cypress-cucumber-preprocessor/blob/master/docs/quick-start.md):

8. Create `cypress.config.ts` according to the tutorial above, but replace
`import { addCucumberPreprocessorPlugin } from "@badeball/cypress-cucumber-preprocessor";`
with
`import cypressCucumberPreprocessor from "@badeball/cypress-cucumber-preprocessor";`
and add a destructuring assignment below the import statements:
`const { addCucumberPreprocessorPlugin } = cypressCucumberPreprocessor;`

9. Follow the above guide further to add some sample tests, or add your own `.feature` and `.ts` files into`cypress/e2e`.
10. `npx cypress open`.
11. When prompted, select e2e testing.

## Disclaimer
Use at your own risk.